class Todo {
    String titulo
    String description
    
    Todo(def tit, def des) {
        titulo = tit
        description = des
    }    
}

todos = []
todos.add(new Todo("Lavadora", "Poner lavadora"))
todos.add(new Todo("Impresora", "Comprar cartuchos impresora"))
todos.add(new Todo("Peliculas", "Devolver peliculas videoclub"))
todos.each {
println it.titulo + " " + it.description
}