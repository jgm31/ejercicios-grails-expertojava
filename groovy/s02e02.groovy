class Calculadora {

    Integer operador1
    Integer operador2
    
    Double calcula(operador1, operador2, operacion) {
        switch(operacion) {
            case(1): return operador1 + operador2; break
            case(2): return operador1 - operador2; break
            case(3): return operador1 * operador2; break
            case(4): return operador1 / operador2; break
        }
    }
   
    
    def introduceNumero = { mensaje ->

       int numero
       def entrada = System.console().readLine mensaje
       numero = Integer.parseInt(entrada.trim())
       return numero
    }
    
    def eligeOperacion(){
        def mensaje =  """\n¿Que operacion deseas? 
        Pulsa 1 para sumar 
        Pulsa 2 para restar 
        Pulsa 3 para multiplicar 
        Pulsa 4 para dividir\n"""
        def opcion = introduceNumero(mensaje)
        
        return opcion 
    }
    
   
    
    int on() {
        
        def operacion = eligeOperacion()
        operador1 = introduceNumero("Introduce primer operador: ")
        operador2 = introduceNumero("Introduce segundo operador: ")
        Double resultado
        resultado = calcula(operador1, operador2, operacion)
        println resultado
        return resultado
    }        
    
    
    static main(args){
        Calculadora calculadora  = new Calculadora();
        calculadora.on()
    }  
}