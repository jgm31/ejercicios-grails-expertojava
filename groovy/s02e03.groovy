/* Añade aquí la implementación solicitada en el ejercicio */


def EN = "£"
def US = "\$"
def ES = "€"

Number.metaClass.moneda = { locale ->
    
    if(locale == "en_EN")
        EN.concat(delegate.toString())
    else if (locale == "en_US")
        US.concat(delegate.toString())
    else if(locale == "es_ES")
        delegate.toString().concat(ES)
}

assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"