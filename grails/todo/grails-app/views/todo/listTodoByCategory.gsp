<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<div id="edit-todo" class="content scaffold-edit" role="main">
    <h1>Seleccionar categorias</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${todoInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${todoInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form url="[resource:todoInstance, action:'listTodoByCategory']">
        <table>
            <thead>
            <tr>
                <g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'nombre')}" />
            </tr>
            </thead>
            <tbody>
            <g:each in="${listTodosByCategorySelected}" status="i" var="todoInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                    <td>${fieldValue(bean: todoInstance, field: "title")}</td>

                </tr>
            </g:each>

            </tbody>
        </table>
        <fieldset class="buttons">
            <g:link class="edit" action="listTodoByCategory" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
            <g:actionSubmit class="delete" action="listTodoByCategory" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
        </fieldset>
    </g:form>
</div>
<a href="#edit-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="listByCategory" action="listByCategory">Mostrar tareas seleccionadas</g:link></li>
    </ul>
</div>
</body>
</html>