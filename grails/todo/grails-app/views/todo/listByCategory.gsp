<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="listByCategory" action="listByCategory"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="list-todo" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${todoInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${todoInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
<g:form url="[resource:todoInstance, action:'listTodoByCategory']">

    <table>
        <thead>
        <tr>
            <g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />
        </tr>
        </thead>
        <tbody>
        <g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:checkBox name="categoria" id="${fieldValue(bean: categoryInstance, field: "id")}" value="${fieldValue(bean: categoryInstance, field: "id")}"/>${fieldValue(bean: categoryInstance, field: "name")}</td>
            </tr>
        </g:each>

        </tbody>
    </table>
    <fieldset class="buttons">
        <g:link class="edit" action="listTodoByCategory" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
        <g:actionSubmit class="list" action="listTodoByCategory" value="${message(code: 'default.button.list.label', default: 'ListByCategory')}" />
        <g:actionSubmit class="update" action="listLastTodosDone" value="Ultimas tareas terminadas en las ultimas 4 horas" />
    </fieldset>
</g:form>
</div>
</body>
</html>