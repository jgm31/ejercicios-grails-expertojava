package es.ua.expertojava.todo

class Todo {
    String title
    String description
    Date date
    Date reminderDate
    String url
    Category category
    Boolean done
    Date dateCreated
    Date lastUpdated
    Date dateDone


    static hasMany = [tags:Tag]
    static belongsTo = [Tag]  //se encarga de los borrados en cascada

    static constraints = {
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        reminderDate(nullable:true,
                validator: { val, obj ->
                    if (val && obj.date) {
                        return val.before(obj?.date)
                    }
                    return true
                }
        )
        url(nullable:true, url:true)
        category(nullable:true)
        done(blank:false, done:false)
        dateDone(nullable:true)
    }
    public Todo(String name, Date date) {
        this.title = name
        this.date = date
    }

    public Todo(String name, Date date, boolean done) {
        this.title = name
        this.date = date
        this.done = done
    }

    String toString(){
        title
    }
}
