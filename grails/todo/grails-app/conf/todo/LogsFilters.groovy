package todo

class LogsFilters {

    def filters = {
        all(controller:'todo|category|tag', action:'create|edit|index|show') {
            before = {

            }
            after = { Map model ->
                log.trace("Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}")
            }
            afterView = { Exception e ->

            }
        }
    }
}
