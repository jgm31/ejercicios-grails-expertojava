import es.ua.expertojava.todo.*

class BootStrap {

    //todo envolver todo en un try catch por la propiedad de color
    def init = { servletContext ->
        def categoryHome = new Category(name:"Hogar").save()
        def categoryJob = new Category(name:"Trabajo").save()

        def tagEasy = new Tag(name:"Fácil").save()
        def tagDifficult = new Tag(name:"Difícil").save()
        def tagArt = new Tag(name:"Arte").save()
        def tagRoutine = new Tag(name:"Rutina").save()
        def tagKitchen = new Tag(name:"Cocina").save()

        def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date()+1, done:false)
        def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date()+2, done:false)
        def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date()+4, done: false)
        def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(), done:true)


        todoPaintKitchen.addToTags(tagDifficult)
        todoPaintKitchen.addToTags(tagArt)
        todoPaintKitchen.addToTags(tagKitchen)
        todoPaintKitchen.category = categoryHome
//        todoPaintKitchen.done = true;
        todoPaintKitchen.save()

        todoCollectPost.addToTags(tagRoutine)
        todoCollectPost.category = categoryJob
        todoCollectPost.save()

        todoBakeCake.addToTags(tagEasy)
        todoBakeCake.addToTags(tagKitchen)
        todoBakeCake.category = categoryHome
        todoBakeCake.save()

        todoWriteUnitTests.addToTags(tagEasy)
        todoWriteUnitTests.category = categoryJob
        todoWriteUnitTests.save()
    }
    def destroy = {
    }
}
