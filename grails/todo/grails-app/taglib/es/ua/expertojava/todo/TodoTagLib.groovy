package es.ua.expertojava.todo

class TodoTagLib {
//    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "todo"

    def printIconFromBoolean = { attributes ->
        if(attributes['value'])
            out<<asset.image(src:"ok-green.png")
        else if(!attributes ['value']) {
            out << asset.image(src: "ko-red.png")
        }
    }
}
