package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    @Unroll
    def "la fecha de recordatorio nunca pueda ser posterior a la fecha de la propia tarea fecha incorrecta"() {
        given:
        def todo = new Todo()
        todo.date= date
        todo.reminderDate=reminderDate

        when:
        todo.validate()

        then:
        todo?.errors['reminderDate']

        where:
        date             |   reminderDate
        new Date()       |   new Date()
        new Date()       |   new Date() + 2

    }

    def "la fecha de recordatorio nunca pueda ser posterior a la fecha de la propia tarea fecha correcta"() {
        given:
        def todo = new Todo()
        todo.date= new Date()
        todo.reminderDate = new Date() - 1

        when:
        todo.validate()

        then:
        !todo?.errors['reminderDate']

    }



    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}
